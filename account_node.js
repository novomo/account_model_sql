const IP = require('ip');
const tableBuilder = require("../../sql_table_builder/builder");
const upload_error = require("../../../node_error_functions/upload_error")


module.exports = async (session, db) => {
  try{
    const account_cols = {
      columns: [
        {
          label: "accountID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "userID", type: "INT(11) UNSIGNED" },
        { label: "name", type: "VARCHAR(25)" },
        { label: "website", type: "VARCHAR(100)" },
        { label: "enabled", type: "Boolean" },
        { label: "minWithdrawal", type: "DOUBLE" },
        { label: "accountType", type: "VARCHAR(100)" },
        { label: "lastUpdated", type: "INT" },
        { label: "commission", type: "DOUBLE" },
      ],
      primary_key: "PRIMARY KEY (accountID)",
      foreign_key: "FOREIGN KEY (userID) REFERENCES users (userID)",
    };

    const balance_cols = {
      columns: [
        {
          label: "balanceID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "userID", type: "INT(11) UNSIGNED" },
        { label: "accountID", type: "INT(11) UNSIGNED" },
        { label: "currency", type: "VARCHAR(4)" },
        { label: "amount", type: "DOUBLE" },
        { label: "conversion", type: "DOUBLE" },
        { label: "currencyType", type: "VARCHAR(10)" },
        { label: "lastUpdated", type: "INT" },
      ],
      primary_key: "PRIMARY KEY (balanceID)",
      foreign_key:
        "FOREIGN KEY (userID) REFERENCES users (userID), FOREIGN KEY (accountID) REFERENCES accounts (accountID)",
    };

    const transaction_cols = {
      columns: [
        {
          label: "transactionID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "userID", type: "INT(11) UNSIGNED" },
        { label: "accountID", type: "INT(11) UNSIGNED" },
        { label: "balanceID", type: "INT(11) UNSIGNED" },
        { label: "amount", type: "DOUBLE" },
        { label: "status", type: "VARCHAR(10)" },
        { label: "lastUpdated", type: "INT" },
      ],
      primary_key: "PRIMARY KEY (transactionID)",
      foreign_key:
        "FOREIGN KEY (userID) REFERENCES users (userID), FOREIGN KEY (accountID) REFERENCES accounts (accountID), FOREIGN KEY (balanceID) REFERENCES balances (balanceID)",
    };

    await tableBuilder(account_cols, "in4freedom", "accounts", session);
    await tableBuilder(balance_cols, "in4freedom", "balances", session);
    await tableBuilder(transaction_cols, "in4freedom", "transactions", session);
} catch(err) {
    console.log(err)
    upload_error({
      errorTitle: "Creating/Editing Accounts table",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
}
};
